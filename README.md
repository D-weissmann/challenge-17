# Challenge 17

# Overview
This was a review to see if the Vine program influenced the number of 5 star reviews on digital books. Unfortunately, this was hindered because there were 0 vine reviews in my dataset...

# Results
1. There were 0 vine reviews and a total of 4,092,489 total nonvine reviews
2. There were a total of 1,251,633 votes for 5 star products. 
3. approximately 35% of the products were rated 5 stars 0% of which were vine reviews. 

# Summary
This review was slightly ruined by the fact that there were 0 vine reviews. 
